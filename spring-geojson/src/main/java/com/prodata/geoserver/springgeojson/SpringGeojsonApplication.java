package com.prodata.geoserver.springgeojson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGeojsonApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringGeojsonApplication.class, args);
    }

}
