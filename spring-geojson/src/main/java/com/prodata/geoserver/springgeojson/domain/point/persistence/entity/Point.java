package com.prodata.geoserver.springgeojson.domain.point.persistence.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prodata.geoserver.springgeojson.serializer.CustomGeometrySerializer;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import org.locationtech.jts.geom.MultiPoint;

@Data
@Entity
@Table(name = "points", schema = "qgislayer")
public class Point {

    @Id
    @Column(name = "id_0")
    private Integer id;

    @Column(name = "id")
    private Long idField;

    @Column(name = "name")
    private String name;

    @Column(name = "geom")
    @JsonSerialize(using = CustomGeometrySerializer.class)
    private MultiPoint geometry;
}
