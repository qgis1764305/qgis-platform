package com.prodata.geoserver.springgeojson.domain.point.persistence.repository;

import com.prodata.geoserver.springgeojson.domain.point.persistence.entity.Point;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointRepository extends JpaRepository<Point, Long> {

}
