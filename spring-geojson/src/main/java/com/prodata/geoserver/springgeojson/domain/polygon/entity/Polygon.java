package com.prodata.geoserver.springgeojson.domain.polygon.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prodata.geoserver.springgeojson.serializer.CustomGeometrySerializer;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import org.locationtech.jts.geom.MultiPolygon;

import java.io.Serializable;

@Data
@Entity
@Table(name = "polygon", schema = "qgislayer")
public class Polygon implements Serializable {

    @Id
    @Column(name = "id_0")
    private Integer id;

    @Column(name = "id")
    private Long idField;

    @Column(name = "name")
    private String name;

    @Column(name = "geom")
    @JsonSerialize(using = CustomGeometrySerializer.class)
    private MultiPolygon geometry;
}
