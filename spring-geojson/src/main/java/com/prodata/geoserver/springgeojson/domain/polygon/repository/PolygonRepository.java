package com.prodata.geoserver.springgeojson.domain.polygon.repository;

import com.prodata.geoserver.springgeojson.domain.polygon.entity.Polygon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PolygonRepository extends JpaRepository<Polygon, Long> {

}
