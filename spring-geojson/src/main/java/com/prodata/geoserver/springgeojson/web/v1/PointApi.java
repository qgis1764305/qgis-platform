package com.prodata.geoserver.springgeojson.web.v1;

import com.prodata.geoserver.springgeojson.domain.point.persistence.entity.Point;
import org.locationtech.jts.geom.MultiPoint;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/point")
public interface PointApi {

    @RequestMapping(
            method = {RequestMethod.GET},
            value = {"/"},
            produces = {"application/json"}
    )
    @CrossOrigin
    @ResponseBody
    List<Point> getAllPoint();
}
