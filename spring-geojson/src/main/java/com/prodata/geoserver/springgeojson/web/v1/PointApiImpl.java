package com.prodata.geoserver.springgeojson.web.v1;


import com.prodata.geoserver.springgeojson.domain.point.persistence.entity.Point;
import com.prodata.geoserver.springgeojson.domain.point.persistence.repository.PointRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PointApiImpl implements PointApi {

    private final PointRepository pointRepository;

    @Override
    public List<Point> getAllPoint() {
        return pointRepository.findAll();
    }


}
