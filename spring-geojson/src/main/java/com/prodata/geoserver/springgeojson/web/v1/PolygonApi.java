package com.prodata.geoserver.springgeojson.web.v1;

import com.prodata.geoserver.springgeojson.domain.polygon.entity.Polygon;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/polygon")
public interface PolygonApi {

    @RequestMapping(
            method = {RequestMethod.GET},
            value = {"/"},
            produces = {"application/json"}
    )
    @CrossOrigin
    @ResponseBody
    public List<Polygon> getAllPolygon();

}
