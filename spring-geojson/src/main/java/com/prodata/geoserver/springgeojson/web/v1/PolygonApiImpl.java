package com.prodata.geoserver.springgeojson.web.v1;

import com.prodata.geoserver.springgeojson.domain.polygon.entity.Polygon;
import com.prodata.geoserver.springgeojson.domain.polygon.repository.PolygonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequiredArgsConstructor
public class PolygonApiImpl implements PolygonApi {

    private final PolygonRepository polygonRepository;

    @Override
    public List<Polygon> getAllPolygon() {
        return polygonRepository.findAll();
    }
}
